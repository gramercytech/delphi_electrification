var sectionData = {};

sectionData["PTE"] = {
	1 : {
		title: " ", 
		content: {
			images: [ 
				{
					fileName: "Electrification_vehicle_main", 
					class: "full-width", 
					css: {
						width: "66%",
						top: "8%", 
						left: "26%"
					}, 
					animation: { name: "fadeIn", delay: 0}
				},				
				{
					fileName: "PTE_01_02", 
					class: "", 
					css: {
						width: "35%",
						top: "7%", 
						left: "3%"
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "electrification_buttons@2x", 
					class: "full-width", 
					css: {
						width: "50%",
						left: "32%", 
						top: "14%"
					}, 
					animation: { name: "fadeIn", delay: 150}
				},
				{
					fileName: "new/logo-delphi@2x", 
					css: {
						width: "16%", 
						left: "4%", 
						top: "79%"
					}, 
					animation: { name: "zoomIn", delay: 150}
				}, 
				{
					fileName: "new/trapezoid", 
					css: {
						width: "100%",
						top: "3%",
						left: 0, 
						"z-index": -1
					}, 
					animation: { name: "fadeIn", delay: 0}
				}
			], 
			buttons: [
				{
					id: "pte_btn",
					class: "slide-btn",
					css: {
						width: "41%", 
						height: "10%",
						left: 0, 
						top: "6%"
					}
				},
				{
					id: "mh_btn", 
					class: "slide-btn",
					css: {
					    width: "10%",
					    height: "22%",
					    top: "27%",
					    left: "33%"
					}
				},
				{
					id: "dsf_btn", 
					class: "slide-btn",
					css: {
					    width: "10%",
					    height: "22%",
					    top: "32%",
					    left: "45%"
					}
				},
				// {
				// 	id: "ea_btn", 
				// 	class: "slide-btn",
				// 	css: {
				// 	    width: "10%",
				// 	    height: "22%",
				// 	    top: "9%",
				// 	    left: "54%"
				// 	}
				// },
				{
					id: "ele_btn", 
					class: "slide-btn",
					css: {
					    width: "10%",
					    height: "22%",
					    top: "19%",
					    left: "67%"
					}
				}

			]
		}, 
		footer: "" 
	},
	2 : {
		title: "Market primarily drive by regulation", 
		content: {
			images: [
				{
					fileName: "PTE_02_01", 
					class: "", 
					css: {
						left: 43,
						top: 185,
						width: "39%"
					}, 
					animation: { name: "slideInLeft", delay: 50}
				}, 
				{
					fileName: "PTE_02_02",
					class: "", 
					css: {
						right: 200,
						top: 182,
						width: 674
					}, 
					animation: { name: "slideInRight", delay: 50}
				}
			]
		}, 
		footer: "<span class='one-line'>Providing integrated intelligent solutions for our customers</span>" 
	},
	3 : {
		title: "Electrification provides more power & closes regulation gap", 
		content: {
			images: [
				{
					fileName: "PTE_03_01A",
					class: "col-xs-12",
					css: {
						top: 185, 
						width: 1332, 
						left: 230
					}, 
					animation: { name: "bounceInDown", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	4 : {
		title: "Light-duty hybrids expected to grow", 
		content: {
			images: [
				{
					fileName: "PTE_04_01", 
					class: "",
					css: {
						left: 25, 
						top: 154, 
						width: 1377
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				}, 
				{
					fileName: "PTE_04_02", 
					class: "",
					css: {
						right: 0, 
						top: 278, 
						width: 339
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			]
		}, 
		footer: "More than <strong>95%</strong> of all light-duty will have an internal combustion engine in 2025" 
	},
	5 : {
		title: "Customers embracing power electronics", 
		content: {
			images: [
				{
					fileName: "PTE_05_01@2x", 
					class: "col-xs-7 left",
					css: {
					    top: 147,
					    width: 1139,
					    left: -50
					},
					animation: { name: "bounceInLeft", delay: 0}
				},
				{
					fileName: "PTE_05_02@2x", 
					class: "col-xs-5 right",
					css: {
						width: 554, 
						right: 80
					},
					animation: { name: "zoomInDown", delay: 100}
				}
			]
		}, 
		footer: "<span class='one-line'>...and selecting Delphi Technologies as the preferred power electronics supplier</span>" 
	},
	6 : {
		title: "Advanced Power Electronics for Powertrain Electrification", 
		content: {
			images: [
				{
					fileName: "PTE_06_01@2x", 
					class: "col-xs-4", 
					css: {
						left: 100,
						width: 430,
						top: 160
					}, 
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "PTE_06_02@2x", 
					class: "col-xs-4", 
					css: {
						right: 712,
						width: 320,
						top: 210
					}, 
					animation: { name: "slideInLeft", delay: 0}
				},
				{
					fileName: "PTE_06_03@2x", 
					class: "col-xs-4", 
					css: {
						right: 50,
						width: 500,
						top: 170
					}, 
					animation: { name: "zoomIn", delay: 0}
				}			
			]
		}, 
		footer: "" 
	}
};

// sectionData["EA"] = {
// 	1 : {
// 		title: "What is a vehicle's electrical architecture?", 
// 		content: {
// 			images: [
// 				{
// 					fileName: "EA_01_01",
// 					class: "",
// 					css: {
// 						left: 17, 
// 						top: 176,
// 						width: 590
// 					}, 
// 					animation: { name: "fadeInDown", delay: 0}
// 				},
// 				{
// 					fileName: "EA_01_02",
// 					class: "",
// 					css: {
// 						right: 60, 
// 						top: 196, 
// 						width: 1030
// 					}, 
// 					animation: { name: "fadeIn", delay: 0}
// 				}, 
// 				{
// 					fileName: "car_mask",
// 					class: "car-mask no-animate"
// 				}
// 			], 
// 			videos: [
// 				{
// 					id: "HSD_1",
// 					fileName: "Delphi HSD US 1.mp4", 
// 					autoplay: true, 
// 					cover: false, 
// 					loop: true
// 				}
// 			]
// 		}, 
// 		footer: "<span class='one-line'>Delphi Technologies is <strong>the</strong> market leader in electrical architecture solutions.</span>" 
// 	},
// 	2 : {
// 		title: "The electrical architecture of today...", 
// 		content: {
// 			images: [
// 				{
// 					fileName: "EA_01_02",
// 					class: "",
// 					css: {
// 						right: 60, 
// 						top: 196, 
// 						width: 1030
// 					}, 
// 					animation: { name: "fadeIn", delay: 0}
// 				}, 
// 				{
// 					fileName: "car_mask",
// 					class: "car-mask no-animate"
// 				},
// 				{
// 					fileName: "Today_01_01@2x",
// 					class: "",
// 					css: {
// 						left: 17, 
// 						top: 176,
// 						width: 590
// 					}, 
// 					animation: { name: "fadeInDown", delay: 0}
// 				}
// 			],
// 			videos: [
// 				{
// 					id: "HSD_2",
// 					fileName: "Delphi HSD US 2.mp4", 
// 					autoplay: true, 
// 					cover: false
// 				}
// 			]
// 		}, 
// 		footer: "<strong>30%</strong> of Delphi Technologies global engineering workforce is focused<br>exclusively on software development."
// 	},
// 	3 : {
// 		title: "The electrical architecture of tomorrow...", 
// 		content: {
// 			images: [
// 				{
// 					fileName: "Tomorrow_01_01@2x",
// 					class: "",
// 					css: {
// 						left: 17, 
// 						top: 176,
// 						width: 590
// 					}, 
// 					animation: { name: "fadeInDown", delay: 0}
// 				},
// 				{
// 					fileName: "EA_01_02",
// 					class: "",
// 					css: {
// 						right: 60, 
// 						top: 196, 
// 						width: 1030
// 					}, 
// 					animation: { name: "fadeIn", delay: 0}
// 				}, 
// 				{
// 					fileName: "car_mask",
// 					class: "car-mask no-animate"
// 				}
// 			], 
// 			videos: [
// 				{
// 					id: "HSD_3",
// 					fileName: "Delphi HSD US 3.mp4", 
// 					autoplay: true, 
// 					cover: false
// 				}
// 			]
// 		}, 
// 		footer: "Delphi Technologies is the key software provider projecting to ship more<br>than <strong>200 billion</strong> lines of code per day by 2020."
// 	},
// 	4 : {
// 		title: "Future solutions enabled by industry-leading architectures", 
// 		content: {
// 			images: [
// 				{
// 					fileName: "Tomorrow_02_01",
// 					class: "",
// 					css: {
// 						left: -63, 
// 						top: 396,
// 						width: 360
// 					}, 
// 					animation: { name: "zoomInLeft", delay: 150}
// 				},
// 				{
// 					fileName: "Tomorrow_02_02",
// 					class: "",
// 					css: {
// 						right: 270, 
// 						top: 156, 
// 						width: 1250
// 					}, 
// 					animation: { name: "zoomInDown", delay: 0}
// 				},
// 				{
// 					fileName: "Tomorrow_02_03",
// 					class: "",
// 					css: {
// 						right: -75, 
// 						top: 376, 
// 						width: 431
// 					}, 
// 					animation: { name: "zoomInRight", delay: 150}
// 				}
// 			]
// 		}, 
// 		footer: "<span class='one-line'>High speed data enables tomorrow's technology today</span>"
// 	},
// 	5 : {
// 		title: "The Delphi Technologies difference...", 
// 		content: {
// 			images: [
// 				{
// 					fileName: "TDD_01_01",
// 					class: "",
// 					css: {
// 						left: 17, 
// 						top: 176,
// 						width: 590
// 					}, 
// 					animation: { name: "fadeInDown", delay: 0}
// 				},
// 				{
// 					fileName: "EA_01_02",
// 					class: "",
// 					css: {
// 						right: 60, 
// 						top: 196, 
// 						width: 1030
// 					}, 
// 					animation: { name: "fadeIn", delay: 0}
// 				}, 
// 				{
// 					fileName: "car_mask",
// 					class: "car-mask no-animate"
// 				}
// 			], 
// 			videos: [
// 				{
// 					id: "HSD_4",
// 					fileName: "Delphi HSD US 4.mp4", 
// 					autoplay: true, 
// 					cover: false
// 				}
// 			]
// 		}, 
// 		footer: "Only Delphi Technologies has <strong>Velocity<sup>TM</sup></strong>, a suite of proprietary tools designed to manage vehicle<br>complexity, developed with more than 50 years of vehicle systems experience."
// 	},
// 	6 : {
// 		title: "Electrical architecture evolution beyond 2025", 
// 		content: {
// 			images: [
// 				{
// 					fileName: "TDD_03_01",
// 					class: "full-width",
// 					css: {
// 						width: "100%",
// 						left: -55
// 					}, 
// 					animation: { name: "slideInLeft", delay: 150}
// 				}
// 			]
// 		}, 
// 		footer: "<span class='one-line'>Future solutions enabled by industry-leading architectures</span>"
// 	},
// };
sectionData["MH"] = {
	1 : {
		title: "It all starts with the electrical architecture", 
		content: {
			images: [
				{
					fileName: "AER_01_01@2x",
					class: "",
					css: {
						width: "29%",
						top: "19%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRight", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_02",
					fileName: "Delphi 48V_02.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	2 : {
		title: "An electrified vehicle", 
		content: {
			images: [
				{
					fileName: "AER_02_01",
					class: "",
					css: {
						width: "28%",
						top: "18%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRight", delay: 0}
				},
				{
					fileName: "Delphi-48V_03_blocker", 
					css: {
						width: 639,
						right: 264,
						top: 575,
   						"z-index": 10000
					}
				}
			],
			videos: [
				{
					id: "48V_03",
					fileName: "Delphi 48V_03.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	3 : {
		title: "An enjoyable ride", 
		content: {
			images: [
				{
					fileName: "AER_03_01",
					class: "",
					css: {
						width: "27%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRight", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_04",
					fileName: "Delphi 48V_04.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	4 : {
		title: "An enjoyable ride", 
		content: {
			images: [
				{
					fileName: "AER_04_01",
					class: "",
					css: {
						width: "27%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRight", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_05",
					fileName: "Delphi 48V_05.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	5 : {
		title: "A greener choice", 
		content: {
			images: [
				{
					fileName: "AGC_01_01",
					class: "",
					css: {
						width: "24%",
						top: "19%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeft", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRight", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_06",
					fileName: "Delphi 48V_06.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	6 : {
		title: "Best-value option", 
		content: {
			images: [
				{
					fileName: "TBVO_01_01",
					class: "",
					css: {
						width: "26%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "TBVO_01_02",
					class: "",
					css: {
						width: "60%",
						top: "23%",
						right: "2%"
					},
					animation: { name: "slideInRight", delay: 120}
				}
			]
		}, 
		footer: "" 
	},
	7 : {
		title: "Best-value option", 
		content: {
			images: [
				{
					fileName: "TBVO_02_01",
					class: "",
					css: {
						width: "27%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "TBVO_02_02",
					class: "",
					css: {
						width: "30%",
						top: "14%",
						right: "10%"
					},
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "TBVO_02_03",
					class: "",
					css: {
						width: "21%",
						top: "7%",
						right: "10%"
					},
					animation: { name: "zoomInDown", delay: 150}
				},
				{
					fileName: "TBVO_02_04",
					class: "",
					css: {
						width: "17%",
						top: "61%",
						right: "9%"
					},
					animation: { name: "zoomInRight", delay: 120}
				},
				{
					fileName: "TBVO_02_05",
					class: "",
					css: {
						width: "21%",
						top: "30%",
						right: "25%"
					},
					animation: { name: "zoomInLeft", delay: 175}
				}
			],
			// videos: [
			// 	{
			// 		id: "48V_07",
			// 		fileName: "Delphi 48V_07.mp4", 
			// 		autoplay: true, 
			// 		cover: false
			// 	}
			// ]
		}, 
		footer: "" 
	},
	8 : {
		title: "The Delphi Technologies solution", 
		content: {
			images: [
				{
					fileName: "TDS_01_01",
					class: "",
					css: {
						width: "27%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},	
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_08",
					fileName: "Delphi 48V_08.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	9 : {
		title: "The Delphi Technologies solution", 
		content: {
			images: [
				{
					fileName: "TDS_02_01",
					class: "",
					css: {
						width: "27%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},	
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_09",
					fileName: "Delphi 48V_09.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	10 : {
		title: "The Delphi Technologies solution", 
		content: {
			images: [
				{
					fileName: "TDS_04_01@2x",
					class: "",
					css: {
						width: "27%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},	
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_10",
					fileName: "Delphi 48V_10.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	},
	11 : {
		title: "The Delphi Technologies solution", 
		content: {
			images: [
				{
					fileName: "TDS_05_01",
					class: "",
					css: {
						width: "27%",
						top: "21%",
						left: "1%"
					},
					animation: { name: "zoomIn", delay: 0}
				},	
				{
					fileName: "VWC_01_02a@2x", 
					class: "MH-top-line",
					css: {
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "MH-btm-line",
					css: {
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			],
			videos: [
				{
					id: "48V_12",
					fileName: "Delphi 48V_12.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true
				}
			]
		}, 
		footer: "" 
	}
}; 
sectionData["DSF"] = {
	1 : {
		title: "Dynamic Skip Fire", 
		content: {
			buttons: [ 
				{
					id: "DSF_vid_btn",
					class: "video-btn",
					target: "DSF_video",
					css: {}
				}
			],
			videos: [
				{
					id: "DSF_video",
					fileName: "DSF.mp4"
				}
			],
			images: [
				{
					fileName: "Dynamic Skip Fire video@2x", 
					class: "video-img",
					css: {
						width: "65%", 
						left: "16.5%", 
						top: "16.7%"
					},
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "VWC_01_02a@2x", 
					class: "",
					css: {
						width: "53%",
						left: "-5%", 
						top: "15%"
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "",
					css: {
						width: "60%",
						left: "45%", 
						top: "90%"
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			]
		}, 
		footer: "" 
	}, 
	2 : {
		title: "Dynamic Skip Fire Introduction", 
		content: {
			images: [
				{
					fileName: "INTRO_01_01", 
					class: "", 
					css: {
						left: 17, 
						top: 176,
						width: 570
					}, 
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "INTRO_01_02@2x", 
					class: "", 
					css: {
						right: 68, 
						top: 129,
						width: 1070
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}
			]
		}, 
		footer: "<span class='one-line'>Dynamic Skip Fire: Silicon Valley Meets Automotive</span>"
	}, 
	3 : {
		title: "Dynamic Skip Fire Introduction", 
		content: {
			images: [
				{
					fileName: "INTRO_02_01", 
					class: "", 
					css: {
						left: 17, 
						top: 176,
						width: 730
					}, 
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "INTRO_02_02", 
					class: "", 
					css: {
						right: 180, 
						top: 183,
						width: 640
					}, 
					animation: { name: "slideInDown", delay: 0}
				}
			]
		}, 
		footer: ""
	}, 
	4 : {
		title: "Dynamic Skip Fire Introduction", 
		content: {
			images: [
				{
					fileName: "INTRO_03_01", 
					class: "", 
					css: {
						left: 17, 
						top: 176,
						width: 590
					}, 
					animation: { name: "zoomIn", delay: 0}
				},
				{
					fileName: "INTRO_03_02", 
					class: "", 
					css: {
						right: 30, 
						top: 243,
						width: 1060
					}, 
					animation: { name: "slideInLeft", delay: 0}
				}
			]
		}, 
		footer: "<span class='one-line'>Technology dynamically adapts to changing conditions</span>"
	}, 
	5 : {
		title: "DSF Valvetrain Hardware: Roller Finger Follower", 
		content: {
			images: [
				{
					fileName: "HW_01_01", 
					class: "", 
					css: {
						left: 38, 
						top: 253,
						width: 430
					}, 
					animation: { name: "zoomIn", delay: 100}
				},
				{
					fileName: "HW_01_02", 
					class: "", 
					css: {
						right: 490, 
						top: 218,
						width: 790
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "HW_01_03", 
					class: "", 
					css: {
						right: 603, 
						top: 172,
						width: 602
					}, 
					animation: { name: "zoomIn", delay: 100}
				},
				{
					fileName: "HW_01_04", 
					class: "", 
					css: {
						right: 323, 
						top: 503,
						width: 291
					}, 
					animation: { name: "zoomIn", delay: 150}
				},
				{
					fileName: "HW_01_05", 
					class: "", 
					css: {
						right: 596, 
						top: 513,
						width: 267
					}, 
					animation: { name: "zoomIn", delay: 150}
				},
				{
					fileName: "HW_01_06@2x", 
					class: "", 
					css: {
						right: 119, 
						top: 150,
						width: 343
					}, 
					animation: { name: "slideInRight", delay: 100}
				}
			]
		}, 
		footer: ""
	}, 
	6 : {
		title: "DSF Valvetrain Hardware: Deactivation Control Valve", 
		content: {
			images: [
				{
					fileName: "HW_02_01", 
					class: "", 
					css: {
						left: 38, 
						top: 253,
						width: 430
					}, 
					animation: { name: "slideInLeft", delay: 150}
				},
				{
					fileName: "HW_02_02@2x", 
					class: "", 
					css: {
						right: 490, 
						top: 218,
						width: 790
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "HW_02_03", 
					class: "", 
					css: {
						right: 646, 
						top: 172,
						width: 352
					}, 
					animation: { name: "zoomIn", delay: 100}
				},
				{
					fileName: "HW_02_04@2x", 
					class: "", 
					css: {
						right: 13, 
						top: 259,
						width: 461
					}, 
					animation: { name: "slideInRight", delay: 150}
				}
			]
		}, 
		footer: ""
	}, 
	7 : {
		title: "Engine Management System", 
		content: {
			images: [
				{
					fileName: "HW_03_01@2x", 
					class: "", 
					css: {
						left: -22, 
						top: 172,
						width: 550
					}, 
					animation: { name: "slideInLeft", delay: 150}
				},
				{
					fileName: "HW_03_02@2x", 
					class: "", 
					css: {
						right: 190, 
						top: 235,
						width: 1030
					}, 
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "HW_03_04@2x", 
					class: "", 
					css: {
						right: 583, 
						top: 254,
						width: 571
					}, 
					animation: { name: "zoomIn", delay: 100}
				},
				{
					fileName: "HW_03_05@2x", 
					class: "", 
					css: {
						left: 310, 
						top: 563,
						width: 230
					}, 
					animation: { name: "zoomIn", delay: 150}
				},
				{
					fileName: "HW_03_06@2x", 
					class: "", 
					css: {
						left: 560, 
						top: 563,
						width: 227, 
						"z-index": -1
					}, 
					animation: { name: "zoomIn", delay: 150}
				},
				{
					fileName: "HW_03_03", 
					class: "", 
					css: {
						right: 60, 
						top: 177,
						width: 434
					}, 
					animation: { name: "slideInRight", delay: 150}
				},
				{
					fileName: "HW_03_07@2x", 
					class: "", 
					css: {
						right: 100, 
						top: 540,
						width: 592
					}, 
					animation: { name: "slideInLeft", delay: 125}
				}
			]
		}, 
		footer: ""
	}, 
	8 : {
		title: "Dynamic Skip Fire", 
		content: {
			buttons: [ 
				{
					id: "DSF_vid_btn",
					class: "video-btn",
					target: "DSF_DC_video",
					css: {}
				}
			],
			videos: [
				{
					id: "DSF_DC_video",
					fileName: "DynamicSkipFire_DriveCycle.mp4"
				}
			],
			images: [
				{
					fileName: "DASH_01_01", 
					class: "video-img",
					css: {
						width: "65.7%", 
						left: "16.2%", 
						top: "20.5%"
					},
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "VWC_01_02a@2x", 
					class: "",
					css: {
						width: "53%",
						left: "-5%", 
						top: "15%"
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "",
					css: {
						width: "60%",
						left: "45%", 
						top: "90%"
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			]
		}, 
		footer: "" 
	}, 
};
sectionData["ELE"] = {
	1 : {
		title: "Path to electrification content per vehicle", 
		content: {
			buttons: [
				{
					class: "slide-btn sysint",
					css: {
						width: 330, 
						height: 335,
						left: 130,
						top: 320
					}
				},
				{
					class: "slide-btn hev",
					css: {
						width: 330, 
						height: 335,
						left: 545,
						top: 320
					}
				},
				{
					class: "slide-btn phev",
					css: {
						width: 330, 
						height: 335,
						left: 950,
						top: 320
					}
				},
				{
					class: "slide-btn ev",
					css: {
						width: 330, 
						height: 335,
						right: 80,
						top: 320
					}
				}
			],
			images: [
				{
					fileName: "Path_01_01",
					class: "",
					css: {
						top: 250,
    					left: 155
					},
					animation: { name: "fadeIn", delay: 0}
				},
				{
					fileName: "Path_01_02",
					class: "",
					css: {
						top: 320,
    					left: 65
					},
					animation: { name: "fadeIn", delay: 0}
				},
			]
		}, 
		footer: "" 
	}, 
	2 : {
		title: "48-Volt, Mild Hybrid", 
		content: {
			buttons: [
				{
					class: "slide-btn",
					fragment: 0,
					css: {
						width: 270, 
						height: 125,
						left: 575,
						top: 660, 
						"z-index": 1
					}
				},
				{
					class: "slide-btn",
					fragment: 1,
					css: {
						width: 270, 
						height: 125,
						left: 853,
						top: 660, 
						"z-index": 1
					}
				},
				{
					class: "slide-btn",
					fragment: 2,
					css: {
						width: 270, 
						height: 125,
						left: 1140,
						top: 660, 
						"z-index": 1
					}
				},
				{
					class: "slide-btn",
					fragment: 3,
					css: {
						width: 270, 
						height: 125,
						left: 1413,
						top: 660, 
						"z-index": 1
					}
				}
			], 
			// html: [
			// 	{
			// 		type: "ul", 
			// 		class: "side-text",
			// 		text: [
			// 			"1. Battery Management Controller", 
			// 			"2. DC/DC Converter", 
			// 			"3. Supervisory Controller", 
			// 			"4. Inverter for Belt-Starter-Generator"
			// 		]
			// 	}
			// ],
			fragments: [
				{
					title: '48-Volt, Mild Hybrid',
					order: 1, 
					images: [
						{
							fileName: "popups/MildHybrid_car_HybridButton", 
							class: "electrification-car", 
							css: {}
						},
						{
							fileName: "popups/48V_01_01@2x", 
							class: "electrification-list", 
							css: {}							
						},
						{
							fileName: "SubMenu_Active_1", 
							class: "electrification-footer", 
							css: {}							
						}
					],
					buttons: [
						{
							class: "slide-btn circle",
							popup: {
								id: "SI_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1540,
								top: 220
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "SI_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 157
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "SI_2",
								fileName: "popups/48V_01_04@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1540,
								top: 320
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "SI_2",
								fileName: "popups/48V_01_04@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 207
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "SI_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 645,
								top: 260
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "SI_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 260
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "SI_4",	
								fileName: "popups/48V_01_06@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 710,
								top: 270
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "SI_4",	
								fileName: "popups/48V_01_06@2x"
							},
							css: {
								width: 430, 
								height: 70,
								left: 10,
								top: 312
							}
						},
						{
							class: "slide-btn circle disabled",
							popup: {
								id: "SI_5",	
								fileName: "popups/48V_01_07@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 614,
								top: 488
							}
						},
						{
							class: "slide-btn disabled",
							popup: {
								id: "SI_5",	
								fileName: "popups/48V_01_07@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 385
							}
						},
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "SI_6",	
						// 		fileName: "popups/48V_01_08@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 614,
						// 		top: 488
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "SI_6",	
						// 		fileName: "popups/48V_01_08@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 440
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "SI_7",	
						// 		fileName: "popups/48V_01_09@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1490,
						// 		top: 260
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "SI_7",	
						// 		fileName: "popups/48V_01_09@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 70,
						// 		left: 10,
						// 		top: 495
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "SI_8",	
						// 		fileName: "popups/48V_01_10@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1170,
						// 		top: 320
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "SI_8",	
						// 		fileName: "popups/48V_01_10@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 565
						// 	}
						// }
					],
					headerId: 0
				},
				{
					title: 'Full Hybrid',
					order: 2, 
					images: [
						{
							fileName: "popups/FullHybrid_car_HybridButton", 
							class: "electrification-car", 
							css: {}
						},
						{
							fileName: "popups/FH_01_01@2x", 
							class: "electrification-list", 
							css: {}							
						},
						{
							fileName: "SubMenu_Active_2", 
							class: "electrification-footer", 
							css: {}							
						}
					],
					buttons: [
						{
							class: "slide-btn circle",
							popup: {
								id: "HEV_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1497,
								top: 343
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "HEV_2",
								fileName: "popups/FH_01_03_1@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1117,
								top: 293
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "HEV_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 805,
								top: 270
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "HEV_4",	
								fileName: "popups/FH_01_03@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 830,
								top: 380
							}
						},
						{
							class: "slide-btn circle disabled",
							popup: {
								id: "HEV_5",	
								fileName: "popups/FH_01_06@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 614,
								top: 488
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "HEV_6",	
								fileName: "popups/FH_01_05@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1108,
								top: 450
							}
						},
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "HEV_7",	
						// 		fileName: "popups/FH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1108,
						// 		top: 450
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "HEV_8",	
						// 		fileName: "popups/FH_01_07@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1210,
						// 		top: 377
						// 	}
						// },
						{
							class: "slide-btn",
							popup: {
								id: "HEV_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 153
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "HEV_2",
								fileName: "popups/FH_01_03_1@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 203
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "HEV_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 254
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "HEV_4",	
								fileName: "popups/FH_01_03@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 305
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "HEV_5",	
								fileName: "popups/FH_01_06@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 355
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "HEV_6",	
								fileName: "popups/FH_01_05@2x"
							},
							css: {
								width: 457, 
								height: 70,
								left: 10,
								top: 406
							}
						},
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "HEV_7",	
						// 		fileName: "popups/FH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 80,
						// 		left: 10,
						// 		top: 455
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "HEV_8",	
						// 		fileName: "popups/FH_01_07@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 539
						// 	}
						// }
					],
					headerId: 1
				},
				{
					title: 'Plug-in Hybrid',
					order: 3, 
					images: [
						{
							fileName: "popups/Pluginbrid_car_HybridButton", 
							class: "electrification-car", 
							css: {}
						},
						{
							fileName: "popups/PIH_01_01@2x", 
							class: "electrification-list", 
							css: {}							
						},
						{
							fileName: "SubMenu_Active_3", 
							class: "electrification-footer", 
							css: {}							
						}
					],
					buttons: [
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1544,
								top: 333
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_2",
								fileName: "popups/FH_01_03_1@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1117,
								top: 293
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 805,
								top: 270
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_4",	
								fileName: "popups/FH_01_03@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 830,
								top: 380
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_5",	
								fileName: "popups/FH_01_05@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1108,
								top: 450
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_6",	
								fileName: "popups/PIH_01_06@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1343,
								top: 430
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_7",	
								fileName: "popups/PIH_01_04@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1441,
								top: 589
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "PHEV_8",	
								fileName: "popups/PIH_01_05@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1441,
								top: 532
							}
						}, 
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "PHEV_9",	
						// 		fileName: "popups/PIH_01_06@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1343,
						// 		top: 430
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "PHEV_10",	
						// 		fileName: "popups/PIH_01_04@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1441,
						// 		top: 589
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "PHEV_11",	
						// 		fileName: "popups/PIH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1441,
						// 		top: 541
						// 	}
						// },
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 154
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_2",
								fileName: "popups/FH_01_03_1@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 204
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 254
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_4",	
								fileName: "popups/FH_01_03@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 305
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_5",	
								fileName: "popups/FH_01_05@2x"
							},
							css: {
								width: 430, 
								height: 80,
								left: 10,
								top: 355
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_6",	
								fileName: "popups/PIH_01_06@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 435
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_7",	
								fileName: "popups/PIH_01_04@2x"
							},
							css: {
								width: 430, 
								height: 55,
								left: 10,
								top: 485
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "PHEV_8",	
								fileName: "popups/PIH_01_05@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 540
							}
						}, 
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "PHEV_9",	
						// 		fileName: "popups/PIH_01_06@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 590
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "PHEV_10",	
						// 		fileName: "popups/PIH_01_04@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 640
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "PHEV_11",	
						// 		fileName: "popups/PIH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 689
						// 	}
						// }
					],
					headerId: 1
				},
				{
					title: 'Electric Vehicle',
					order: 4, 
					images: [
						{
							fileName: "popups/ElectricHybrid_car_HybridButton", 
							class: "electrification-car", 
							css: {}
						},
						{
							fileName: "popups/EV_01_01@2x", 
							class: "electrification-list", 
							css: {}							
						},
						{
							fileName: "SubMenu_Active_4", 
							class: "electrification-footer", 
							css: {}							
						}
					],
					buttons: [
						{
							class: "slide-btn circle",
							popup: {
								id: "EV_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1422,
								top: 276
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "EV_2",
								fileName: "popups/FH_01_03_1@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 1117,
								top: 293
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "EV_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 805,
								top: 270
							}
						},
						{
							class: "slide-btn circle",
							popup: {
								id: "EV_4",	
								fileName: "popups/FH_01_03@2x"
							},
							css: {
								width: 50, 
								height: 50,
								left: 830,
								top: 380
							}
						},
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "EV_5",	
						// 		fileName: "popups/FH_01_06@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 780,
						// 		top: 194
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "EV_6",	
						// 		fileName: "popups/FH_01_04@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 945,
						// 		top: 247
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "EV_7",	
						// 		fileName: "popups/FH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1108,
						// 		top: 450
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "EV_8",	
						// 		fileName: "popups/FH_01_07@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 908,
						// 		top: 328
						// 	}
						// }, 
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "EV_9",	
						// 		fileName: "popups/PIH_01_06@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1343,
						// 		top: 430
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "EV_10",	
						// 		fileName: "popups/PIH_01_04@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1441,
						// 		top: 589
						// 	}
						// },
						// {
						// 	class: "slide-btn circle",
						// 	popup: {
						// 		id: "EV_11",	
						// 		fileName: "popups/PIH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 50, 
						// 		height: 50,
						// 		left: 1441,
						// 		top: 541
						// 	}
						// },
						{
							class: "slide-btn",
							popup: {
								id: "EV_1",
								fileName: "popups/48V_01_03@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 150
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "EV_2",
								fileName: "popups/FH_01_03_1@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 200
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "EV_3",
								fileName: "popups/48V_01_05@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 250
							}
						},
						{
							class: "slide-btn",
							popup: {
								id: "EV_4",	
								fileName: "popups/FH_01_03@2x"
							},
							css: {
								width: 430, 
								height: 50,
								left: 10,
								top: 300
							}
						},
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "EV_5",	
						// 		fileName: "popups/FH_01_06@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 350
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "EV_6",	
						// 		fileName: "popups/FH_01_04@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 400
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "EV_7",	
						// 		fileName: "popups/FH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 90,
						// 		left: 10,
						// 		top: 450
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "EV_8",	
						// 		fileName: "popups/FH_01_07@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 540
						// 	}
						// }, 
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "EV_9",	
						// 		fileName: "popups/PIH_01_06@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 590
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "EV_10",	
						// 		fileName: "popups/PIH_01_04@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 640
						// 	}
						// },
						// {
						// 	class: "slide-btn",
						// 	popup: {
						// 		id: "EV_11",	
						// 		fileName: "popups/PIH_01_05@2x"
						// 	},
						// 	css: {
						// 		width: 430, 
						// 		height: 50,
						// 		left: 10,
						// 		top: 690
						// 	}
						// }
					],
					headerId: 2
				}
			]
		}, 
		footer: "" 
	}
};

sectionData["ADD"] = {
	1 : {
		title: "Electrified Vehicle is growing in<br>the Light Duty Vehicle segment",
		content: {
			images: [
				{
					fileName: "M_01_01",
					class: "full-width",
					css: {
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	2 : {
		title: "Electrified Vehicles grow 10X from 2016 to 2025",
		content: {
			images: [
				{
					fileName: "M_02_01@2x",
					class: "full-width",
					css: {
						width: "97%",
						left: "3%",
						top: "25%"
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	3: {
		title: "Electrified Vehicle by region market trends",
		content: {
			images: [
				{
					fileName: "M_03_01",
					class: "full-width",
					css: {
						width: "94%"
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	4 : {
		title: "The 48V Market is predicted to<br>grow 10X from 2019 to 2025",
		content: {
			images: [
				{
					fileName: "M_04_01",
					class: "full-width",
					css: {
						width: "77%"
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	5 : {
		title: "48V Mild Hybrid forecase by region",
		content: {
			images: [
				{
					fileName: "M_05_01@2x",
					class: "full-width",
					css: {
						width: "94%"
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	6 : { // VALUE ANALYSIS
		title: "Delphi Technologies 48V Mild Hybrid Value Proposition",
		content: {
			images: [
				{
					fileName: "M_06_01",
					class: "full-width",
					css: {
						width: "59%",
						left: "23%"
					},
					animation: { name: "zoomInDown", delay: 0}
				},
				{
					fileName: "M_06_02@2x",
					class: "",
					css: {
						width: "20%",
						left: "1%", 
						top: "10%"
					},
					animation: { name: "slideInLeft", delay: 150}
				}
			]
		}, 
		footer: "" 
	}, 
	7 : {
		title: "Delphi Technologies 48V Mild Hybrid Value Proposition",
		content: {
			images: [
				{
					fileName: "M_07_02_2@2x",
					class: "full-width",
					css: {
						width: "59%",
						left: "23%"
					},
					animation: { name: "zoomInDown", delay: 0}
				},
				{
					fileName: "M_07_02@2x",
					class: "",
					css: {
						width: "17%",
						left: "1%", 
						top: "10%"
					},
					animation: { name: "slideInLeft", delay: 150}
				}
			]
		}, 
		footer: "" 
	},
	8 : { // 48V DEMO RESULTS
		title: "Delphi Technologies 48V Mild Hybrid Value + Dynamic Skip<br>Fire (DSF) VW Passat demo vehicle",
		content: {
			images: [
				{
					fileName: "M_07_01A@2x",
					class: "full-width",
					css: {
						width: "106%",
						left: -68,
						top: 180
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	9 : {
		title: "Delphi Technologies 48V Mild Hybrid Honda Civic<br>demo vehicle",
		content: {
			images: [
				{
					fileName: "M_08_01A@2x",
					class: "full-width",
					css: {
						width: "106%",
						left: -68,
						top: 180
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	10 : {
		title: "Delphi Technologies 48V Mild Hybrid ChangAn CS75<br>demo vehicle",
		content: {
			images: [
				{
					fileName: "M_14_01@2x",
					class: "full-width",
					css: {
						width: "106%",
						left: -68,
						top: 180
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	11 : {
		title: "Delphi Technologies Dynamic Skip Fire (DSF) VW Jetta demo vehicle",
		content: {
			images: [
				{
					fileName: "M_15_01@2x",
					class: "full-width",
					css: {
						width: "106%",
						left: -68,
						top: 180
					},
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	12 : {
		title: "Delphi Technologies Intelligent Driving VW Passat Demo Plan",
		content: {
			images: [
				{
					fileName: "M_09_01-2x-cutout",
					class: "full-width no-animate",
					css: {
						width: "105%",
						left: -136,
						top: 207,
						"z-index": 1
					}
				}, 
				{
					fileName: "M_09_02@2x",
					class: "full-width",
					css: {
						top: "87%"
					},
					animation: { name: "slideInLeft", delay: 150}
				}
			], 
			videos: [
				{
					id: "HSD_1_2",
					fileName: "Delphi HSD US 1.mp4", 
					autoplay: true, 
					cover: false, 
					loop: true	
				}
			]
		}, 
		footer: "" 
	},
	13 : { // DSF DEMO
		title: "Value Proposition - V8 engines",
		content: {
			images: [
				{
					fileName: "M_10_02@2x",
					class: "",
					css: {
						width: "62%",
						left: "37%",
						top: "18%"
					},
					animation: { name: "slideInDown", delay: 150}
				}, 
				{
					fileName: "M_10_01@2x",
					class: "",
					css: {
						top: "17%",
						width: "30%",
						left: "1%"
					},
					animation: { name: "slideInDown", delay: 0}
				}
			]
		}, 
		footer: "" 
	},
	14 : {
		title: "Value Proposition - Four cylinder engines",
		content: {
			images: [
				{
					fileName: "M_11_02",
					class: "",
					css: {
						width: "60%",
						left: "37%",
						top: "18%"
					},
					animation: { name: "slideInDown", delay: 150}
				}, 
				{
					fileName: "M_11_01",
					class: "",
					css: {
						top: "17%",
						width: "30%",
						left: "1%"
					},
					animation: { name: "slideInDown", delay: 0}
				}
			]
		}, 
		footer: "" 
	}, 
	15 : { // TYPES OF PROPULSION
		title: "Light-duty hybrids expected to grow", 
		content: {
			images: [
				{
					fileName: "PTE_04_01", 
					class: "",
					css: {
						left: 25, 
						top: 154, 
						width: 1377
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				}, 
				{
					fileName: "PTE_04_02", 
					class: "",
					css: {
						right: 0, 
						top: 283, 
						width: 339
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			]
		}, 
		footer: "More than <strong>95%</strong> of all light-duty will have an internal combustion engine in 2025"
	}, 
	16 : { // POWER ELECTRONICS PORTFOLIO
		title: "High Voltage: Delphi Technologies Value Proposition", 
		content: {
			images: [
				{
					fileName: "PEP_05_01@2x", 
					class: "",
					css: {
						left: -85, 
						top: 113, 
						width: 617
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				}, 
				{
					fileName: "PEP_05_02@2x", 
					class: "",
					css: {
						right: 0, 
						top: 150, 
						width: 1239
					}, 
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: ""
	}, 
	17 : {
		title: "Advanced Power Electronics for Powertrain Electrification", 
		content: {
			images: [
				{
					fileName: "PEP_02_01@2x", 
					class: "",
					css: {
						left: -85, 
						top: 133, 
						width: 747
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				}, 
				{
					fileName: "PEP_02_02@2x", 
					class: "",
					css: {
						right: 490, 
						top: 160, 
						width: 600
					}, 
					animation: { name: "fadeInDownBig", delay: 0}
				}, 
				{
					fileName: "PEP_02_03@2x", 
					class: "",
					css: {
						right: 60, 
						top: 170, 
						width: 420
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}, 
				{
					fileName: "PEP_02_04@2x", 
					class: "",
					css: {
						right: 50, 
						top: 550, 
						width: 480
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			]
		}, 
		footer: ""
	}, 
	18 : {
		title: "Viper Gen1 IGBT Switch", 
		content: {
			buttons: [ 
				{
					id: "Viper_vid_btn",
					class: "video-btn",
					target: "Viper_video",
					css: {}
				}
			],
			videos: [
				{
					id: "Viper_video",
					fileName: "Viper_16x9_ROUGH CUT_v01.mp4"
				}
			],
			images: [
				{
					fileName: "VPR_img_2A", 
					class: "video-img",
					css: {
						width: "65.4%", 
						left: "16.2%", 
						top: "22%"
					},
					animation: { name: "fadeIn", delay: 0}
				}, 
				{
					fileName: "VWC_01_02a@2x", 
					class: "",
					css: {
						width: "53%",
						left: "-5%", 
						top: "15%"
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				},
				{
					fileName: "VWC_01_02b@2x", 
					class: "",
					css: {
						width: "60%",
						left: "45%", 
						top: "90%"
					}, 
					animation: { name: "fadeInRightBig", delay: 0}
				}
			]
		},
		footer: ""
	}, 
	19 : {
		title: "Advanced Power Electronics for Powertrain Electrification", 
		content: {
			images: [
				{
					fileName: "PEP_03_01@2x", 
					class: "col-xs-4",
					css: {
						width: "22%",
						top: "21%",
						left: "5%"
					},
					animation: { name: "zoomInUp", delay: 0}
				},
				{
					fileName: "PEP_03_02@2x", 
					class: "col-xs-4",
					css: {
						top: "21%",
						width: "19%", 
						left: "41%"					
					},
					animation: { name: "zoomIn", delay: 100}
				},
				{
					fileName: "PEP_03_03@2x", 
					class: "col-xs-4",
					css: {
						right: "4%",
						width: "25%",
						top: "22%"
					},
					animation: { name: "zoomInUp", delay: 0}
				}
			]
		},
		footer: ""
	}, 
	20 : {
		title: "48V: Delphi Technologies Value Proposition", 
		content: {
			images: [
				{
					fileName: "PEP_04_01@2x", 
					class: "",
					css: {
						left: -85, 
						top: 113, 
						width: 617
					}, 
					animation: { name: "fadeInLeftBig", delay: 0}
				}, 
				{
					fileName: "PEP_04_02@2x", 
					class: "",
					css: {
						right: 0, 
						top: 150, 
						width: 1239
					}, 
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: ""
	}, 
	21 : {
		title: "800V Inverter Technology", 
		content: {
			images: [
				{
					fileName: "PEP_01_01 2", 
					css: {
						left: 90, 
						top: 170
					}, 
					animation: { name: "zoomInLeft", delay: 0}
				},
				{
					fileName: "PEP_01_02", 
					css: {
						left: 500,
						top: 170, 
						width: 337
					}, 
					animation: { name: "zoomInRight", delay: 0}
				},
				{
					fileName: "PEP_01_03", 
					css: {
						right: 110, 
						top: 180
					}, 
					animation: { name: "zoomIn", delay: 0}
				}
			]
		}, 
		footer: ""
	}
	// 21 : { // ELECTRICAL ARCHITECTURE PORTFOLIO
	// 	title: "Delphi Technologies electrification architectures", 
	// 	content: {
	// 		images: [
	// 			{
	// 				fileName: "DEA_01_01_1@2x", 
	// 				class: "",
	// 				css: {
	// 					left: 25, 
	// 					top: 113, 
	// 					width: 1617
	// 				}, 
	// 				animation: { name: "fadeInLeftBig", delay: 0}
	// 			}, 
	// 			{
	// 				fileName: "DEA_01_02@2x", 
	// 				class: "",
	// 				css: {
	// 					right: 60, 
	// 					bottom: -23, 
	// 					width: 1639
	// 				}, 
	// 				animation: { name: "fadeInRightBig", delay: 0}
	// 			}, 
	// 			{
	// 				fileName: "DEA_01_04@2x", 
	// 				class: "",
	// 				css: {
	// 					left: 20, 
	// 					top: 130, 
	// 					width: 439
	// 				}, 
	// 				animation: { name: "fadeInRightBig", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 22 : {
	// 	title: "Hybrid and Electric Vehicle Market Projections", 
	// 	content: {
	// 		images: [
	// 			{
	// 				fileName: "M_02_01@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "97%",
	// 					left: "3%", 
	// 					top: "25%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 23 : {
	// 	title: "PHEV and EV Market Projections", 
	// 	content: {
	// 		images: [
	// 			{
	// 				fileName: "PHEV_01_02@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "93%",
	// 					left: "3%", 
	// 					top: "18%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 24 : {
	// 	title: "High Power Connection Systems Product Line", 
	// 	content: {
	// 		images: [
	// 			{
	// 				fileName: "HPCS_01_01@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "98%",
	// 					left: "-2%", 
	// 					top: "20%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			},
	// 			{
	// 				fileName: "HPCS_01_02@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "97%",
	// 					left: "1%", 
	// 					top: "51%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			},
	// 			{
	// 				fileName: "HPECPL_01_03@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "50%",
	// 					left: "1%", 
	// 					top: "12%"
	// 				}, 
	// 				animation: { name: "slideInLeft", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 25 : {
	// 	title: "High Power Wiring Architecture Product Line", 
	// 	content: {
	// 		images: [
	// 			{
	// 				fileName: "HPWAPL_01_01@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "98%",
	// 					left: "3%", 
	// 					top: "20%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			},
	// 			{
	// 				fileName: "HPWAPL_01_02@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "97%",
	// 					left: "1%", 
	// 					top: "51%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			},
	// 			{
	// 				fileName: "HPECPL_01_03@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "50%",
	// 					left: "1%", 
	// 					top: "12%"
	// 				}, 
	// 				animation: { name: "slideInLeft", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 26 : {
	// 	title: "High Power Electrical Center Product Line", 
	// 	content: {
	// 		images: [
	// 			{
	// 				fileName: "HPECPL_01_01@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "83%",
	// 					left: "5%", 
	// 					top: "15%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			},
	// 			{
	// 				fileName: "HPECPL_01_02@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "88%",
	// 					left: "1%", 
	// 					top: "44%"
	// 				}, 
	// 				animation: { name: "zoomIn", delay: 0}
	// 			},
	// 			{
	// 				fileName: "HPECPL_01_03@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "50%",
	// 					left: "1%", 
	// 					top: "12%"
	// 				}, 
	// 				animation: { name: "slideInLeft", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 27 : {
	// 	title: "Delphi Technologies Cordset Volume Projection", 
	// 	content: {
	// 		images: [
	// 			{
	// 				fileName: "DCVP_01_01@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "82%",
	// 					left: "0%", 
	// 					top: "24%"
	// 				}, 
	// 				animation: { name: "slideInLeft", delay: 0}
	// 			},
	// 			{
	// 				fileName: "DCVP_01_02@2x",
	// 				css: {
	// 					right: 0, 
	// 					bottom: 150, 
	// 					width: 275
	// 				}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 28 : {
	// 	title: "PHEV & EV Charging Sustained Innovation Over Time", 
	// 	content: {
	// 		images: [
	// 			{
	// 					fileName: "PHEV_01_03@2x", 
	// 					class: "",
	// 					css: {
	// 						width: "92%",
	// 						left: "1%", 
	// 						top: "15%"
	// 					}, 
	// 					animation: { name: "zoomInLeft", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }, 
	// 29 : {
	// 	title: "Vehicle Wireless Charging", 
	// 	content: {
	// 		buttons: [ 
	// 			{
	// 				id: "DSF_vid_btn",
	// 				class: "video-btn",
	// 				target: "Wireless_video",
	// 				css: {}
	// 			}
	// 		],
	// 		videos: [
	// 			{
	// 				id: "Wireless_video",
	// 				fileName: "Wireless_Charging.mp4"
	// 			}
	// 		],
	// 		images: [
	// 			{
	// 				fileName: "VWC_01_01@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "24%",
	// 					left: "39%", 
	// 					top: "18%"
	// 				}, 
	// 				animation: { name: "fadeIn", delay: 0}
	// 			},
	// 			{
	// 				fileName: "VWC_01_02a@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "53%",
	// 					left: "-5%", 
	// 					top: "15%"
	// 				}, 
	// 				animation: { name: "fadeInLeftBig", delay: 0}
	// 			},
	// 			{
	// 				fileName: "VWC_01_02b@2x", 
	// 				class: "",
	// 				css: {
	// 					width: "60%",
	// 					left: "45%", 
	// 					top: "90%"
	// 				}, 
	// 				animation: { name: "fadeInRightBig", delay: 0}
	// 			}
	// 		]
	// 	}, 
	// 	footer: ""
	// }
};

	