var navData = {
	"buttons" : [
		// {
		// 	id: "ea-btn",
		// 	text: "ELECTRICAL ARCHITECTURE", 
		// 	goTo: "#EA_1"
		// },
		{
			id: "mh-btn",
			text: "48V MILD HYBRID", 
			goTo: "#MH_1"
		}, 
		{
			id: "dsf-btn",
			text: "DYNAMIC SKIP FIRE", 
			goTo: "#DSF_1"
		}, 
		{
			id: "ele-btn",
			text: "ELECTRIFICATION", 
			goTo: "#AFI_1"
		}
	], 
	"additional" : [
		{
			id: "market-btn",
			text: "MARKET", 
			goTo: "#ADD_0"
		}, 
		{
			id: "va-btn",
			text: "VALUE ANALYSIS",
			goTo: "#ADD_5"
		},
		{
			id: "48v-btn",
			text: "48V DEMO",
			goTo: "#ADD_7"
		}, 
		{
			id: "dsf-btn",
			text: "DSF DEMO",
			goTo: "#ADD_11"
		}, 
		{
			id: "prop-btn",
			text: "TYPES OF PROPULSION",
			goTo: "#ADD_13"
		}, 
		{
			id: "pep-btn",
			text: "POWER ELECTRONICS<br>PORTFOLIO",
			goTo: "#ADD_14"
		}
		// {
		// 	id: "eap-btn",
		// 	text: "ELECTRICAL ARCHITECTURE<br>PORTFOLIO",
		// 	goTo: "#ADD_19"
		// }
	]
}