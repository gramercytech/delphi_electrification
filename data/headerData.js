var headerData = {
	// "ea-header" : [
	// 	{
	// 		title: "ELECTRICAL ARCHITECTURE"
	// 	},
	// 	{
	// 		title: "TODAY"
	// 	},
	// 	{
	// 		title: "TOMORROW"
	// 	},
	// 	{
	// 		title: "THE DELPHI TECHNOLOGIES DIFFERENCE"
	// 	}
	// ],
	"mh-header" : [
		{
			title: "AN ENJOYABLE RIDE"
		},
		{
			title: "A GREENER CHOICE"
		},
		{
			title: "THE BEST-VALUE OPTION"
		},
		{
			title: "THE DELPHI TECHNOLOGIES SOLUTION"
		}
	],
	"dsf-header" : [
		{
			title: "VIDEO"
		},
		{
			title: "INTRODUCTION"
		},
		{
			title: "HARDWARE"
		},
		{
			title: "DASHBOARD"
		}
	],
	"ele-header" : [
		{
			title: "CONTENT PER VEHICLE"
		}
	]
};