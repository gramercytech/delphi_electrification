'use strict';

var $ = require('jQuery');
const reveal = require('reveal');

class SlideRenderer {
    constructor(container) {
        this.html = container;
    }

    title (text) {
      var title = $("<h1>", {class: "h1 main-title active"});
      title.html(text);

      this.html.append(title);
    }

    subtitle (text) {
        var title = $("<h2>", {class: "h2 sub-title"});
        title.html(text);

        this.html.append(title);
    }

    footer (text) {
        var footer = $("<div>", {class: "footer-text"});

        var topLine = $("<div>", {class: "top-footer-line"});
        var btmLine = $("<div>", {class: "btm-footer-line"});

        var footerText = $("<p>");      

        var topImg = $('<img>', {src: "assets/new/Footer_1@2x.png"});
        var btmImg = $('<img>', {src: "assets/new/Footer_2@2x.png"});

        topLine.append(topImg);
        btmLine.append(btmImg);

        footerText.html(text);

        footer.append(topLine);
        footer.append(btmLine);
        footer.append(footerText);

        this.html.append(footer);
    }

    images(images) {
        AddImages(images, this.html);      
    }

    videos(videos, section) {
        for (var i = 0; i < videos.length; i++) {
            if (videos[i].autoplay) {
                // section.attr({
                //     "data-background-video": "assets/videos/" + videos[i].fileName, 
                //     "data-background-video-loop": true
                // })
                
                if (videos[i].cover) {
                    var $videoDiv = $("<div>", {id: videos[i].id, class: "auto-video cover"});
                } else {
                    var $videoDiv = $("<div>", {id: videos[i].id, class: "auto-video side"});
                }

                if (videos[i].loop) {
                    var $video = $('<video loop>');
                } else {
                    var $video = $('<video>');
                }

                $video.attr('src', "assets/videos/" + videos[i].fileName);

                $videoDiv.append($video);
                this.html.append($videoDiv);
            } else {
                var $modalDiv = $("<div>", {id: videos[i].id, class: "modal"}); 
                var $modalClose = $("<button>", {class: "modal-close"}); 
                //var $background = $("<div>", {class: "modal-background"});

                $modalDiv.append($modalClose);
                //$modalDiv.append($background);

                $modalClose.on('click', function() {
                    var modal = $(this).parents('.modal')[0];
                    var video = $(this).siblings('video')[0];
                    $(modal).hide();

                    if ($(modal).attr('id') == "DSF_DC_video") {
                        $('#DSF_8').find('.video-img').show();
                    }

                    $('.footer-menu').removeClass('blur');
                    $('.right-nav').removeClass('blur');
                    $('.left-nav').removeClass('blur');
                    video.load();
                })

                var $video = $('<video>');
                $video.attr('src', "assets/videos/" + videos[i].fileName);

                $modalDiv.append($video);
                this.html.append($modalDiv);
            }
        }   
    }

    buttons(buttons) {
        for (var i = 0; i < buttons.length; i++) {
            var btn = $('<button>', {id: buttons[i].id});

            if (buttons[i].class && buttons[i].class != "") {
              btn.addClass(buttons[i].class);
            }
            // video play btn 
            if (buttons[i].class == "video-btn" || buttons[i].class == "play-video") {
                btn.attr("data-toggle", "modal");
                btn.attr("data-target", buttons[i].target);

                btn.on('click', function() {
                    var targetId = $(this).data().target;
                    var video = $('#' + targetId).find('video')[0];

                    $('#' + targetId).show(); // show video modal

                    // deactivate screen buttons when video is playing
                    $('.footer-menu').addClass('blur');
                    $('.right-nav').addClass('blur');
                    $('.left-nav').addClass('blur');

                    // play video
                    if (targetId == "Viper_video") { // UPDATE: viper video play later
                        video.currentTime = 6.5;
                        video.play();
                    } else {
                        video.load();
                        video.play();
                    }

                    $(video).on('click', function() {
                        if (this.currentTime > 0 && !this.paused) {
                            this.pause();
                        } else {
                            this.play();
                        }
                    })

                    $(video).on('ended', function() {
                        var modal = $(this).parents('.modal')[0];
                        $(modal).hide();
                        $('.footer-menu').removeClass('blur');
                    })

                    $(video).on('ended', function() {
                        var modal = $(this).parents('.modal')[0];
                        $(modal).hide();

                        $('.footer-menu').removeClass('blur');
                        $('.right-nav').removeClass('blur');
                        $('.left-nav').removeClass('blur');
                    })
                })
            } 

            if (buttons[i].hasOwnProperty('fragment')) {
                btn.attr("data-target", buttons[i].fragment);

                btn.on('click', function() {
                    var indices = reveal.getIndices();
                    var frag_index = parseInt($(this).attr("data-target"));

                    reveal.slide(indices.h, indices.v, frag_index);
                })
            }

            if (buttons[i].css) {
                btn.css(buttons[i].css);
            }

            this.html.append(btn);
        }        
    }

    fragments(fragments) {
        for (var i = 0; i < fragments.length; i++) {
            var images = fragments[i].images;
            var index = fragments[i].order;
            var title = fragments[i].title;

            if (index == 1) {
                var f_Div = $('<div>', {class: "fragment current-visible visible"});
            } else {
                var f_Div = $('<div>', {class: "fragment current-visible"});
            }

            if (title) {
                var $h1 = $('<h1>', {class: "h1 main-title", id: "ELE-title-" + i});
                $h1.html(title);
                this.html.append($h1);
            }

            AddImages(images, f_Div);

            if (fragments[i].hasOwnProperty('buttons')) {
                var buttons = fragments[i].buttons;

                AddButtons(buttons, f_Div);
            }

            this.html.append(f_Div);
        }  
    }

    addHTML(html) {
        for (var i = 0; i < html.length; i++) {
            var type = html[i].type;
            var className = html[i].class;
            var text = html[i].text;

            if (type == "ul") {
                var html = $('<ul>', {class: className}); 

                text.forEach(function(e) {
                    var li = $('<li>', {text: e});

                    html.append(li);
                });
            };

            this.html.append(html);
        }
    }
};

function AddImages(images, container) {
    for (var i = 0; i < images.length; i++) {
        var imgDiv = $('<div>', {class: "img-div"});
        var img = $('<img>');

        img.attr('src', "assets/" + images[i].fileName + ".png");

        if (images[i].class && images[i].class != "") {
          imgDiv.addClass(images[i].class);
        }

        // set css styles
        if (images[i].css) {
            imgDiv.css(images[i].css);
        }

        // set animation attribute
        if (images[i].animation) {
            imgDiv.data('animate-name', images[i].animation.name);
            imgDiv.data('animate-delay', images[i].animation.delay);
        } else {
            imgDiv.data('animate-name', "fadeIn");
            imgDiv.data('animate-delay', 100);      
        }

        imgDiv.append(img)
        container.append(imgDiv);
    } 
};

function AddButtons(buttons, container) {
    for (var i = 0; i < buttons.length; i++) {
        var btn = $('<button>');

        // btn.text(buttons[i].popup.id);

        if (buttons[i].class && buttons[i].class != "") {
          btn.addClass(buttons[i].class);
        }

        if (buttons[i].css) {
            btn.css(buttons[i].css);
        }       

        if (buttons[i].popup) {
            // set data target with pop-up ID
            btn.attr("data-target", buttons[i].popup.id);
            btn.attr("id", buttons[i].popup.id + "_btn");
            // create modal div
            AddModal(buttons[i].popup, container);

            if (!btn.hasClass('disabled')) {
                // event listener for modal popup
                btn.on('click', function() {
                    var targetId = $(this).data().target;

                    $('#' + targetId).show(); // show modal

                    // deactivate screen buttons when modal is opened
                    $('.footer-menu').addClass('blur');
                    $('.right-nav').addClass('blur');
                    $('.left-nav').addClass('blur');
                })   
            }         
        }

        container.append(btn); 
    }
};

function AddModal(modal, container) {
    var $modalDiv = $("<div>", {id: modal.id, class: "modal popup"}); 
    var $modalClose = $("<button>", {class: "modal-close"}); 
    var $background = $("<div>", {class: "modal-background"});

    $modalDiv.append($modalClose);
    $modalDiv.append($background);

    $modalClose.on('click', function() {
        var modal = $(this).parents('.modal')[0];

        $(modal).hide();

        $('.footer-menu').removeClass('blur');
        $('.right-nav').removeClass('blur');
        $('.left-nav').removeClass('blur');
    })

    var $content = $('<img>');
    $content.attr('src', "assets/" + modal.fileName + '.png');

    $modalDiv.append($content);
    container.append($modalDiv);
};

module.exports = SlideRenderer;
