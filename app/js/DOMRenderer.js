function renderImages(images, container) {
  for (var i = images.length - 1; i >= 0; i--) {
  	var imgDiv = $('<div>', {class: "img-div"});
    var img = $('<img>');

    img.attr('src', "assets/" + images[i].fileName + ".png");

    if (images[i].class && images[i].class != "") {
      imgDiv.addClass(images[i].class);
    }

    // set css styles
    if (images[i].css) {
    	imgDiv.css(images[i].css);
    }

    // set animation attribute
    if (images[i].animation) {
    	imgDiv.data('animate-name', images[i].animation.name);
    	imgDiv.data('animate-delay', images[i].animation.delay);
    } else {
    	imgDiv.data('animate-name', "fadeIn");
    	imgDiv.data('animate-delay', 100);    	
    }

    imgDiv.append(img)
    container.append(imgDiv);
  }
}

function renderButtons(buttons, container) {
	for (var i = buttons.length - 1; i >= 0; i--) {
		var btn = $('<button>', {id: buttons[i].id});

		if (buttons[i].class && buttons[i].class != "") {
		  btn.addClass(buttons[i].class);
		}

		if (buttons[i].css) {
			btn.css(buttons[i].css);
		}

		container.append(btn);
	}
}

function createTitle(text, container) {
  var title = $("<h1>", {class: "h1 main-title"});
  title.html(text);

  container.append(title);
}

function createFooter() {
    var footer = $("<div>");

    var topLine = $("<div>", {class: "top-footer-line"});
    var btmLine = $("<div>", {class: "btm-footer-line"});

    if (key == 4 || key == 31 || key == 32) {
    	var footerText = $("<p>", {class: "double"});      	
    } else {
    	var footerText = $("<p>");    	
    }

    var topImg = $('<img>', {src: "assets/new/Footer_1@2x.png"});
    var btmImg = $('<img>', {src: "assets/new/Footer_2@2x.png"});

    topLine.append(topImg);
    btmLine.append(btmImg);

    footer.addClass("footer-text");
    footerText.html(slideData[key].footer);

    footer.append(topLine);
    footer.append(btmLine);
    footer.append(footerText);

    section.append(footer);
}