// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js eles are available in this process.
var $ = require('jQuery');

// const electron = require('electron');
// const app = electron.app
// const BrowserWindow = electron.BrowserWindow
const {ipcRenderer} = require('electron');

const Renderer = require('./SlideRenderer.js');

// include jquery animate.css functionality
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.show();
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
        return this;
    }
});

var header = $('.top-header');
var main = $('.slides');

function createSlide(id, data) {
	var section = $("<section>", {id: id});
	var content = data.content;

	var renderer = new Renderer(section);

	renderer.title(data.title);

	if (data.subtitle) {
		renderer.subtitle(data.subtitle);
	}

	if (data.footer != "") {
		renderer.footer(data.footer);
	}

	if (content.videos) {
		renderer.videos(content.videos, section);
	}

	if (content.images) {
		renderer.images(content.images);
	}

	if (content.buttons) {
		renderer.buttons(content.buttons);
	}

	if (content.fragments) {
		renderer.fragments(content.fragments);
	}

	if (content.html) {
		renderer.addHTML(content.html);
	}

	return renderer;
}

Object.keys(sectionData).forEach(function(key) {
	var section = $("<section>", {id: key + "_container"});
	var slideData = sectionData[key];

	for (var slideKey in slideData) {
		var data = slideData[slideKey];
		var slide = createSlide(key + "_" + slideKey, data);

		section.append(slide.html)
	}

	section.attr('data-state', key);
	main.append(section);
});


Object.keys(headerData).forEach(function(key, index) {
	var $ul = $("<ul>", {id: key});

	var data = headerData[key];

	data.forEach(function(elem, i) {
		var $li = $("<li>");
		var title = elem.title;

		$li.html( title );
		$ul.append($li);		
	})

	header.append($ul)
});

Object.keys(navData).forEach(function(key) {
	var footer = $('.footer-menu .navbar-nav');
	var dropUp = $('.dropdown-menu');
	var data = navData[key];

	data.forEach(function(elem, i) {
		if (key == "buttons") { // sets main nav buttons
			var $li = $("<li>");
			var $btn = $("<button>", {class: 'btm-nav-btn', id: elem.id});
			$btn.attr("data-goTo", elem.goTo);
			$btn.text(elem.text);
			$li.append($btn);

			footer.append($li);	
		} else { // sets popup nav buttons
			var $li = $("<li>");
			var $btn = $("<button>", {class: 'additional-nav', id: elem.id});
			$btn.attr("data-goTo", elem.goTo);
			$btn.html(elem.text);
			$li.append($btn);

			dropUp.append($li);
		}	
	});
});

const reveal = require('reveal');

reveal.initialize({
	controls: false,
	keyboard: true,
	touch: false,
	// default/cube/page/concave/zoom/linear/fade/none 
	transition: 'linear',
	width: "100%",
	height: "80%",
	margin: "5%",
	dependencies: [
	 	// { src: '../node_modules/reveal/plugin/zoom-js/zoom.js', async: true }
		// { src: '../node_modules/reveal.js-menu/menu.js', async: true }
	]
})

$('#exit-fs').on('click', function() {
    $( 'body' ).trigger({
        type: 'keypress',
        which: 27,
        keyCode: 27
    });
});

$('#exit-fs').on('dblclick', function() {
	location.reload();
});

$('.btm-nav-btn').on('click', function() {
	var currentSlide = reveal.getCurrentSlide().id.toLowerCase().split('_')[0];
	var goTo = $(this).attr('id').split('-')[0];

	if (currentSlide != goTo) {
		switch (goTo) {
			// case "ea": 
			// 	reveal.slide(1, 0, 0);
			// 	break; 
			case "mh": 
				reveal.slide(1, 0, 0);
				break; 
			case "dsf":
				reveal.slide(2, 0, 0);
				break; 
			case "ele": 
				reveal.slide(3, 0, 0);
				break;
			default: 
				break;
		}
	}
});

$('#home-btn').on('click', function() {
	var currentSlide = reveal.getCurrentSlide();
	var id = currentSlide.getAttribute('id');

	if (id == "PTE_1") { // navigate to Main Menu screen if on first slide
		$('#MM-container').fadeToggle();
	} else { 
		reveal.slide(0, 0);
	}
});

$('#pte_btn').on('click', function() {
	reveal.slide(0, 1);
});

// Electrifiction section first slide buttons
$(".slide-btn.sysint").on('click', function() {
	reveal.slide(3, 1, 0);
});
$(".slide-btn.hev").on('click', function() {
	reveal.slide(3, 1, 1);
});
$(".slide-btn.phev").on('click', function() {
	reveal.slide(3, 1, 2);
});
$(".slide-btn.ev").on('click', function() {
	reveal.slide(3, 1, 3);
});

// Main Menu buttons

// DSF video
$('#dsf-mm-btn').on('click', function() {
	var dsf_vid = $('#DSF-vid-container').find('video')[0];
	var dsf_btn = $('#DSF-vid-container').find('button')[0];

	$('#DSF-vid-container').fadeIn();
	dsf_vid.play();

	$(dsf_btn).on('click', function() {
		var vid = $(this).siblings('video')[0];
		var container = $('#DSF-vid-container');
		vid.load();

		container.fadeOut();
	});	
});

// eDSF button
$('#mh-mm-btn').on('click', function() {
	ipcRenderer.send('open-eDSF', 'eDSF');
});

// Intelligent Driving button
$('#id-mm-btn').on('click', function() {
	ipcRenderer.send('open-intelligent-driving', 'intelligent_driving');
});

// Electrification button
$('#ele-mm-btn').on('click', function() {
	$('#MM-container').fadeToggle();
});

// Slide navigation buttons
$('.rightSlide').on('click', function() {
	var hIndex = reveal.getIndices().h; // 0 => PTE , 1 => EA, 2 => 48VMH, 3 => DSF, 4 => ELE
	var vIndex = reveal.getIndices().v; // 0 => first slide
	var totalSlides = $(reveal.getCurrentSlide()).parent().children().length;

	console.log("h: " + hIndex  + "," + "v: " + vIndex + "," + "total: " + totalSlides);

	if(!$(this).hasClass('disabled')) {
		if (hIndex == 0 && vIndex == 0) { // first slide will navigate to EA section
			reveal.slide(1, 0, 0); 
		} else if (vIndex == totalSlides - 1) { // if slide is last in section, go to next section
			reveal.slide(hIndex + 1, 0, 0); 
		} else {
			reveal.slide(hIndex, vIndex + 1, 0);
		}
	}
})

$('.leftSlide').on('click', function() {
	var hIndex = reveal.getIndices().h; // 0 => PTE , 1 => EA, 2 => 48VMH, 3 => DSF, 4 => ELE
	var vIndex = reveal.getIndices().v; // 0 => first slide

	if(!$(this).hasClass('disabled')) { 
		if (hIndex == 1 && vIndex == 0) {
			reveal.slide(0, 0, 0);
		} else if (vIndex == 0) {
			reveal.prev(); // if first slide, then go to last slide in prev section
		} else {
			reveal.slide(hIndex, vIndex - 1, 0); // go to prev slide in section
		}
	}
})

$('.f_left').on('click', function() {
	var fIndex = reveal.getIndices().f;
	var currentSlide = $(reveal.getCurrentSlide());

	currentSlide.find('.f_right').removeClass('disabled');
	console.log(fIndex);

	if(!$(this).hasClass('disabled')) { 
		reveal.prevFragment();
	}

	if (fIndex == 1) {
		currentSlide.find('.f_left').addClass('disabled');
	}
})

$('.f_right').on('click', function() {
	var fIndex = reveal.getIndices().f;
	var currentSlide = $(reveal.getCurrentSlide());
	var totalFragments = currentSlide.find('.fragment').length;

	currentSlide.find('.f_left').removeClass('disabled');

	if(!$(this).hasClass('disabled')) { 
		reveal.nextFragment();
	}

	if (fIndex == totalFragments - 2) {
		currentSlide.find('.f_right').addClass('disabled');
	}
})

// allows second DSF video to hide background image on play
$('#DSF_DC_video').find('video').on('play', function() {
	$('#DSF_8').find('.video-img').hide();

	$(this).on('ended', function() {
		$('#DSF_8').find('.video-img').show();
	})
})

// FIRST SLIDE BUTTONS
$('#PTE_1 .slide-btn').on('click', function() {
	switch ($(this).attr('id')) {
		// case "ea_btn": 
		// 	reveal.slide(1, 0, 0);
		// 	break;
		case "mh_btn":
			reveal.slide(1, 0, 0);
			break; 
		case "dsf_btn":
			reveal.slide(2, 0, 0);
			break; 
		case "ele_btn": 
			reveal.slide(3, 0, 0);
			break; 
		default: 
			break;
	}
});

// set close + navigate for additional popup
$('.additional-nav').on('click', function() {
	var btn = $(this);
	// var slideId = reveal.getCurrentSlide().id;
	var indices = reveal.getIndices();
	// var sectionTitle = slideId.split('_')[0].toLowerCase();
	if (indices.h != 5) {
		$('#close').show();

		$('#close').off().on('click', function(e) {
			// only go to first slide if in GSS section
			if (indices.h == 0) {
				reveal.slide(0, 0);
			} else {
				if (indices.f > 0) {
					reveal.slide(indices.h, indices.v, indices.f);
				} else {
					reveal.slide(indices.h, indices.v, 0);
				}
			}

			$('#close').hide();
		})
	}

// ****** SETS WHERE POPUP MENU NAVIGATE TO ******** //
	switch (btn.attr('id')) {
		case "market-btn": // MARKET
			reveal.slide(5, 0);
			break;
		case "va-btn": // VALUE ANALYSIS
			reveal.slide(5, 5);
			break; 
		case "48v-btn": // 48V DEMO
			reveal.slide(5, 7);
			break; 
		case "dsf-btn": // DSF DEMO
			reveal.slide(5, 12);
			break; 
		case "prop-btn": // TYPES OF PROPULSION
			reveal.slide(5, 14);
			break;
		case "pep-btn": // POWER ELECTRONICS PORTFOLIO
			reveal.slide(5, 15);
			break; 
		case "eap-btn": // ELECTRICAL ARCHITECTURE PORTFOLIO
			reveal.slide(5, 20);
			break;  
		default: 
			break;
	}
})

function activateButton (name = null) {
	var buttons = $(".footer-menu .navbar-nav").children("li");
	var menuItems = $(".dropdown-menu").children();

	buttons.each(function() {
		if ($(this).children('button').attr('id') == name) {
			$(this).addClass('active');
		} else if (name == "add-btn") {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	})

	menuItems.each(function() {
		var item = $(this).children('button')[0];
		$(item).removeClass('active');
	})
};

function activateDropup (name) {
	var buttons = $(".footer-menu .navbar-nav").children("li");
	var menuItems = $(".dropdown-menu").children();

	buttons.each(function() {
		if ($(this).attr('id') == "dropUp") {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});

	menuItems.each(function() {
		var item = $(this).children('button')[0];

		if ($(item).attr('id') == name ) {
			$(item).addClass('active');
		} else {
			$(item).removeClass('active');
		}
	})
};

function showHeader (type, index) {
	$(".top-header").children('ul').each(function(i, e) {
		if (e.id == type) {
			var subheaders = $(e).children(':not(.main_header)');

			$.each(subheaders, function(ind, el) {
				if (index == ind) {
					$(el).addClass('active');
				} else {
					$(el).removeClass('active');
				}
			});

			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	})
};

function showADDHeader (type, index) {
	$(".top-header").children('ul').each(function(i, e) {
		if (e.id == type) {
			var subheaders = $(e).children(':not(.main_header)');

			$.each(subheaders, function(ind, el) {
				if (index == el.id) {
					$(el).addClass('active');
				} else {
					$(el).removeClass('active');
				}
			});

			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	})
};
function changeTitle (id, slide) { // changes title of slide
	$(slide).children('h1').each(function(i, e) {
		if (e.id == id) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	})
};

reveal.addEventListener( 'fragmenthidden', function( event ) {
	var indices = reveal.getIndices();
	var currentSlide = reveal.getCurrentSlide();

	fragmentEvent(indices, currentSlide);
});

reveal.addEventListener( 'fragmentshown', function( event ) {
	var indices = reveal.getIndices();
	var currentSlide = reveal.getCurrentSlide();

	fragmentEvent(indices, currentSlide);
});

reveal.addEventListener( 'ready', function(e) {
	var slideId = e.currentSlide.id;

	slideEvent(slideId);
	animate(slideId);
});

reveal.addEventListener( 'slidechanged', function(e) {
	var slideId = e.currentSlide.id;

	slideEvent(slideId);
	animate(slideId);
}, false );

// fragment events
function fragmentEvent(indices, slide) {
	if (indices.h == 3 && indices.v == 1) { // electrification first slide
		changeTitle("ELE-title-" + indices.f, slide);
	}
};

// animate slide
function animate(slideId) {
	var slide = $('#' + slideId);
	var slideImages = slide.find('.img-div').not('.no-animate');
	var footer = slide.children('.footer-text');
	var title = slide.find('.main-title');
	// fade in title
	title.animate({opacity: 1}, 1200);

	// animate images
	slideImages.each( function(index, element) {
		var animation = $(this).data('animate-name');
		var delay = $(this).data('animate-delay');

        if (animation) {
        	setTimeout(function() {
        		$(element).children().animateCss(animation);
        	}, delay)
        }
	})
	// animate footer lines
	if (footer.length != 0) {
		var topLine = $(footer).children('.top-footer-line');
		var btmLine = $(footer).children('.btm-footer-line');

		topLine.removeAttr('style');
		btmLine.removeAttr('style');

		topLine.animate({
			width: "100%"
		}, 1500);

		btmLine.animate({
			width: "100%"
		}, 1500);
	}

};
function playVideo(slideId) {
	var videos = $('video');
	var currentSlide = $('#' + slideId);

	videos.each(function (i, e) {
		if ($(e).parent().parent().attr('id') == slideId) {
			e.play();
		} else {
			e.pause();
		}
	});
};
// trigger slide events
function slideEvent(slideId) {
	var sectionTitle = slideId.split('_')[0].toLowerCase();
	var slideNum = parseInt(slideId.split('_')[1]);

	// highlight section button
	activateButton(sectionTitle + "-btn");

	// disable all left buttons to prevent toggling through empty fragments
	var indices = reveal.getIndices();
	var numFragments = $('#' + slideId).find('.fragment').length;

	// inner slide fragment arrow functionality
	if (indices.f == 0) { // if first fragment
		$('#' + slideId).find('.f_left').addClass('disabled');
		$('#' + slideId).find('.f_right').removeClass('disabled');	
	} else if (indices.f == numFragments - 1) { // if last fragment
		$('#' + slideId).find('.f_left').removeClass('disabled');	
		$('#' + slideId).find('.f_right').addClass('disabled');		
	} else {
		$('#' + slideId).find('.f_left').removeClass('disabled');	
		$('#' + slideId).find('.f_right').removeClass('disabled');
	}

	// hide close button if not in ADD or GSS section
	if (sectionTitle == "pte" && slideNum > 1) {
		$('#close').show();

		$('#close').off().on('click', function() {
			reveal.slide(0, 0);
		})
	} else if ((sectionTitle != "add" && sectionTitle != "pte") && $('#close').is(":visible")) {
		$('#close').hide();
	} else if (slideId == "PTE_1") {
		$('#close').hide();
	}

	// disable left arrow
	if (slideId == "PTE_1" || slideId == "ADD_1") {
		$('.leftSlide').addClass('disabled');
	} else {
		$('.leftSlide').removeClass('disabled');
	}

	// disable right arrow
	if (slideId == "ADD_21" || slideId == "PTE_6" || slideId == "ELE_2") {
		$('.rightSlide').addClass('disabled');
	} else {
		$('.rightSlide').removeClass('disabled');
	}

	// toggle correct sub-headers
	switch (sectionTitle) {
		case "pte":

			if (slideNum == 1) {
				showHeader ();
			} else {
				showHeader ("pte-header", 0);
			}
			break;
		// case "ea":
		// 	switch(slideNum) {
		// 		case 1:
		// 			showHeader ("ea-header", 0);
		// 			playVideo(slideId);
		// 			break;
		// 		case 2:
		// 			showHeader ("ea-header", 1);
		// 			playVideo(slideId);
		// 			break;
		// 		case 3:
		// 			showHeader ("ea-header", 2);
		// 			playVideo(slideId);
		// 			break;
		// 		case 4:
		// 			showHeader ("ea-header", 2);
		// 			break;
		// 		case 5:
		// 			showHeader ("ea-header", 3);
		// 			playVideo(slideId);
		// 			break;
		// 		default: 
		// 			showHeader("ea-header", 3)
		// 			break;
		// 	}
		// 	break;
		case "mh":
			var enjoyable_ride = [1, 2, 3, 4];
			var greener_choice = [5];
			var best_value = [6, 7];
			var delphi_solution = [8, 9, 10];

			switch(true) {
				case (enjoyable_ride.includes(slideNum)):
					showHeader ("mh-header", 0);
					break;
				case (greener_choice.includes(slideNum)):
					showHeader ("mh-header", 1);
					break;
				case (best_value.includes(slideNum)):
					showHeader ("mh-header", 2);
					break;
				case (delphi_solution.includes(slideNum)):
					showHeader ("mh-header", 3);
					break;
				default: 
					showHeader("mh-header")
					break;
			}

			playVideo(slideId);
			break;
		case "dsf":
			switch(slideNum) {
				case 1:
					showHeader ("dsf-header", 0);
					break;
				case 2:
					showHeader ("dsf-header", 1);
					break;
				case 3:
					showHeader ("dsf-header", 1);
					break;
				case 4:
					showHeader ("dsf-header", 1);
					break;
				case 5:
					showHeader ("dsf-header", 2);
					break;
				case 6:
					showHeader ("dsf-header", 2);
					break;
				case 7:
					showHeader ("dsf-header", 2);
					break;
				case 8:
					showHeader ("dsf-header", 3);
					break;
				default: 
					showHeader("dsf-header")
					break;
			}
			break;
		case "ele":
			showHeader ("ele-header", 0);				
			break;
		case "add": 
			switch (true) {
				case (slideNum < 6): // slides 1-5
					var name = "market";
					break;
				case (5 < slideNum && slideNum < 8): // slides 6-7
					var name = "va";
					break; 
				case (7 < slideNum && slideNum < 13): // slides 8-13
					var name = "48v";
					if (slideNum == 12) {
						playVideo(slideId);
					}
					break; 
				case (12 < slideNum && slideNum < 15): // slides 13-14
					var name = "dsf";
					break;
				case (slideNum == 15): // slides 15
					var name = "prop";
					break;
				case (15 < slideNum && slideNum < 22): // slides 16-20
					var name = "pep";
					break;
				// case (20 < slideNum && slideNum < 28): // slides 21-29
				// 	var name = "eap";
				// 	break;
				default: 
					console.log("ERROR: MENU BTN NOT FOUND");
			}

			showADDHeader("add-header", name + "-header");

			activateDropup (name + '-btn');
			break;
		default: 
			break;
	}

}
