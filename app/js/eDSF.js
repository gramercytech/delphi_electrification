// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
var $ = require('jQuery');

const remote = require('electron').remote;

var timestamps = {
	1: 0, 
	2: 16, 
	3: 23, 
	4: 41, 
	5: 60, 
	6: 71, 
	7: 76
};

// include jquery animate.css functionality
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.show();

        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
        return this;
    }
});


const reveal = require('reveal');

reveal.initialize({
	controls: false,
	keyboard: true,
	touch: true,
	autoPlayMedia: true,
	// default/cube/page/concave/zoom/linear/fade/none 
	transition: 'none',
	width: "100%",
	height: "80%",
	margin: "5%",
	dependencies: []
})

$('#previous-btn').on('click', function() {
	var slide = reveal.getPreviousSlide();
	var video = $(slide).find('video');

	loadVideo(video);

	reveal.prev();
});

$('#next-btn').on('click', function() {
	reveal.next();
});

$('#play-btn').on('click', function() {
	var slide = reveal.getCurrentSlide();
	toggleVideo(slide);
});

$('video').on('ended', function() {
	var slide = reveal.getCurrentSlide();
	var currentSlide = slide.id;

	if (currentSlide == "slide-7") { // fade last video to white
		$(slide).fadeTo(2000, 0);

		setTimeout(function(){ 
			reveal.next();
			$(slide).fadeTo(2000, 1);
		}, 2000);

	} else { // go to next slide on video end
		reveal.next();
	}
});

$('#navigator-btn').on('click', function() {
	var slide = reveal.getCurrentSlide();

	if (slide.id == "slide-1") {
		var window = remote.getCurrentWindow();
	    window.close();		
	} else {
		var video = $('#slide-1').find('video')[0];

		loadVideo(video);

		reveal.slide(0, 0);
	}
});

$('#reload-btn').on('dblclick', function() {
	location.reload();
});

function toggleVideo(slide) {
	var video = $(slide).find("video")[0];
	var playBtn = $('#play-btn');

	if (!video.paused) {	
		video.pause();
		playBtn.removeClass('active');
	} else {
		video.play();
		playBtn.addClass('active');
	}
};

function hideImages(slide) {
	var slideImages = $(slide).find('img.animate');

	//hide previous slide images
	slideImages.each( function(index, element) {
		$(element).hide();
	});

};

function loadVideo(video) {
	video.currentTime = 0;
};

// animate slide
function animate(slide) {
	var slideImages = $(slide).find('img.animate');

	// animate images
	slideImages.each( function(index, element) {
		var animation = $(this).data('animate-name');
		var delay = $(this).data('animate-delay');

        if (animation) {
        	setTimeout(function() {
        		$(element).animateCss(animation);
        	}, delay)
        }
	})
};

function reloadImage(slide) {
	var slideImages = $(slide).find('img.animate');

	//hide previous slide images
	slideImages.each( function(index, element) {
		$(element).hide();
	});
};

// upon loading page, do this
reveal.addEventListener( 'ready', function(e) {
	var slide = e.currentSlide;
	var slideId = slide.id;

	console.log("presentation starting...");

	// if first slide, play video
	toggleVideo(slide);
	animate(slide);
	// hide prev btn on first slide
	$('#previous-btn').hide();
});

// when changing slides, do this
reveal.addEventListener( 'slidechanged', function(e) {
	var slide = e.currentSlide;
	var prevSlide = e.previousSlide;
	var slideId = slide.id;

	console.log("navigating to " + slideId);

	if (slideId == "slide-3") {
		var blueBox = $(slide).find('.slideFeature');
		$(blueBox).addClass("animated slideInLeft");
	} else if (slideId == "slide-1") {
		$('#previous-btn').hide();
	} else {
		$('#previous-btn').show();
	}

	reloadImage(slide);
	hideImages(prevSlide);
	animate(slide);

	// change play button to pause
	if (!$('#play-btn').hasClass('active')) {
		$('#play-btn').addClass('active');
	}
 	
	if (slideId != "slide-8") {
		$('#next-btn').show();
	} else { // if last slide
		$('#next-btn').hide();
	}
}, false );
